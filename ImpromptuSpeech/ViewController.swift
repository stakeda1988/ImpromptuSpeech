//
//  ViewController.swift
//  ImpromptuSpeech
//
//  Created by SHOKI TAKEDA on 11/3/15.
//  Copyright © 2015 handsomeslot.com. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var countLabel: UILabel!
    var timer:NSTimer!
    var counter:Int = 60
    @IBAction func smallStartButton(sender: UIButton) {
        timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: Selector("onUpdate"), userInfo: nil, repeats: true)
    }
    func onUpdate(){
        counter -= 1
        countLabel.text = String(counter)
        if counter == 0 {
            timer.invalidate()
        }
    }
    @IBOutlet weak var smallStart: UIButton!
    @IBOutlet weak var randomLabel: UILabel!
    var randomGlobal:UInt32 = 0
    //アニメーションの素材
    let img0:UIImage = UIImage(named:"pic1.jpg")!
    let img1:UIImage = UIImage(named:"pic2.jpg")!
    let img2:UIImage = UIImage(named:"pic3.jpg")!
    let img3:UIImage = UIImage(named:"pic4.jpg")!
    let img4:UIImage = UIImage(named:"pic5.jpg")!
    let img5:UIImage = UIImage(named:"pic6.jpg")!
    let img6:UIImage = UIImage(named:"pic7.jpg")!
    let img7:UIImage = UIImage(named:"pic8.jpg")!
    
    @IBOutlet weak var imageView: UIImageView!

    @IBAction func startPause(sender: UIButton) {
        if imageView.isAnimating() {
            imageView.stopAnimating() // アニメが動いてたら止める。
            switch randomGlobal {
                case 1:
                    imageView.image = img0
                case 2:
                    imageView.image = img1
                case 3:
                    imageView.image = img2
                case 4:
                    imageView.image = img3
                case 5:
                    imageView.image = img4
                case 6:
                    imageView.image = img5
                case 7:
                    imageView.image = img6
                case 8:
                    imageView.image = img7
                default:
                    imageView.image = img7
            }
            randomLabel.text = String(randomGlobal)
            smallStart.hidden = false
        } else {
            imageView.startAnimating() // アニメが止まってたら動かす。
            let random = arc4random_uniform(20)
            randomGlobal = random
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let imgArray:[UIImage] = [img0, img1, img2, img3, img4, img5, img6, img7]
        
        //アニメーションのコマの設定
        imageView.animationImages = imgArray;
        //アニメーションのリピート回数の設定（０は無限ループ）
        imageView.animationRepeatCount = 0;
        //コマの切り替わる時間の設定
        imageView.animationDuration = 0.8;
        smallStart.hidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}